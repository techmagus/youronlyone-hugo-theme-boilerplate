# YourOnly.One Hugo Theme Boilerplate

A boilerplate for [Hugo](https://gohugo.io) themes made by [Yelosan Publishing](https://yelosan.youronly.one) and [YourOnly.One](https://im.youronly.one). The theme integrates support for Microformats, a11y (accessibility), ARIA, RDFa, IndieWeb, Webmention, Fediverse, i18n (internationlisation).
